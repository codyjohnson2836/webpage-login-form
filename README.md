**In-progress Web Login Form**

---

Currently includes Login and Create Account webpages. Account creation is supported, and a created account is stored in a database that can be created by running the provided setup.sql file.

---

Planned additions:

1. Validating user logins
2. Creating user login sessions
3. Creating logged in page which allows user to logout
4. Hashing user passwords before storing them in the database
5. Checking whether a given username is already in use by another account