/**
 * Uses a given form's "form__message" element to display a success or
 * error message.
 * @param {Element} formElement - The form to display the message on.
 * @param {String} type - The type of message. Can be either "success" or "error".
 * @param {String} message - The message to be displayed.
 */
function setFormMessage(formElement, type, message) {
    const messageElement = formElement.querySelector(".form__message");

    messageElement.textContent = message;
    messageElement.classList.remove("form__message-success", "form__message-error");
    messageElement.classList.add(`form__message-${type}`);
}

/**
 * Displays an error message underneath the given input element.
 * @param {Element} inputElement - The input element associated with the error.
 * @param {String} message - The error message to be displayed.
 */
function setInputError(inputElement, message) {
    inputElement.classList.add("form__input-error");
    inputElement.parentElement.querySelector(".form__input-error-message").textContent = message;
}

/**
 * Clears the error message associated with an input element.
 * @param {Element} inputElement - The input element to remove the error from.
 */
function clearInputError(inputElement) {
    inputElement.classList.remove("form__input-error");
    inputElement.parentElement.querySelector(".form__input-error-message").textContent = "";
}

/**
 * Checks a given username to see if it fits the following format:
 * 6-30 characters,
 * no spaces,
 * is alphanumeric
 * @param {String} username - The username to be checked.
 * @returns The error message detailing any issues with the username. Is "" if no issues were found.
 */
function checkValidUsername(username) {
    let errorMessage = "";

    if (username.length == 0) {
        errorMessage = "Username is required.";
    } else {
        if (username.length < 6 || username.length > 30) {
            errorMessage += "Username must be between 6–30 characters in length.\n";
        }
        if (/ /.test(username) == true) {
            errorMessage += "Username must not contain any spaces.\n";
        }
        if (!/^[a-zA-Z0-9 ]+$/.test(username)) {
            errorMessage += "Username must be alphanumeric.\n";
        }
    }   

    return errorMessage;
}

/**
 * Checks a given email address to see if it meets a general address format.
 * @param {String} emailAddress - The email address to be checked.
 * @returns An error message stating whether the input was generally valid. Is "" if no issues were found.
 */
function checkValidEmailAddress(emailAddress) {
    let errorMessage = "";

    if (emailAddress.length == 0) {
        errorMessage = "Email address is required.";
    } else if (!/^\S+@\S+\.\S+$/.test(emailAddress) || emailAddress.length > 320) {
        errorMessage = "Email address must be valid.";
    }

    return errorMessage;
}

/**
 * Checks a given password to see if it fits the following format:
 * 10-30 characters,
 * at least 5 letters,
 * at least 4 numbers,
 * at least 1 symbol
 * @param {String} password - The password to be checked.
 * @returns The error message detailing any issues with the password. Is "" if no issues were found.
 */
function checkValidPassword(password) {
    let errorMessage = "";

    if (password.length == 0) {
        errorMessage = "Password is required.";
    } else {
        if (password.length < 10 || password.length > 30) {
            errorMessage += "Password must be between 10–30 characters in length.\n";
        }
        if ((password.match(/[a-zA-Z]/g)||[]).length < 5) {
            errorMessage += "Password must contain at least 5 letters.\n";
        }
        if ((password.match(/\d/g)||[]).length < 4) {
            errorMessage += "Password must contain at least 4 numbers.\n";
        }
        if (/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(password) == false) {
            errorMessage += "Password must contain at least 1 symbol.\n";
        }
    }

    return errorMessage;
}

/**
 * Checks the signupPassword input element and the signupConfirmPassword input element to see if they match.
 * @returns Boolean for whether the two input elements match. Can be true or false.
 */
function checkPasswordMatch() {
    return document.getElementById("signupPassword").value == document.getElementById("signupConfirmPassword").value;
}

// Fires after HTML has finished loading
document.addEventListener("DOMContentLoaded", () => {
    // Load both initially accessible forms
    const loginForm = document.querySelector("#login");
    const createAccountForm = document.querySelector("#createAccount");

    // If "create account" link is clicked, swap form to Create Account form
    document.querySelector("#linkCreateAccount").addEventListener("click", e => {
        e.preventDefault();
        loginForm.classList.add("form-hidden");
        createAccountForm.classList.remove("form-hidden");
    });

    // If "login" link is clicked, swap form to Login form
    document.querySelector("#linkLogin").addEventListener("click", e => {
        e.preventDefault();
        createAccountForm.classList.add("form-hidden");
        loginForm.classList.remove("form-hidden");
    });

    // Events for each input element
    document.querySelectorAll(".form__input").forEach(inputElement => {
        // Fires when input element loses focus
        // Check to see if errors need to be displayed and if so, display them
        inputElement.addEventListener("blur", e => {
            // Check for username errors
            if (e.target.id == "signupUsername") {
                const errorMessage = checkValidUsername(e.target.value);
                if (errorMessage != "") {
                    setInputError(inputElement, errorMessage);
                }
            // Check for email address errors
            } else if (e.target.id == "signupEmailAddress") {
                const errorMessage = checkValidEmailAddress(e.target.value);
                if (errorMessage != "") {
                    setInputError(inputElement, errorMessage);
                }
            // Check for password errors
            } else if (e.target.id == "signupPassword") {
                const errorMessage = checkValidPassword(e.target.value);
                if (errorMessage != "") {
                    setInputError(inputElement, errorMessage);
                }

                // If password does not match confirmation password, display error for confirmation password
                const isMatching = checkPasswordMatch();
                if (isMatching == false) {
                    setInputError(document.getElementById("signupConfirmPassword"), "Passwords must match.");
                } else {
                    clearInputError(document.getElementById("signupConfirmPassword"));
                }
            }
        });

        // Fires when user types in input element
        // Check to see if errors need to be displayed and if so, display them
        inputElement.addEventListener("input", e => {
            // Check for password errors
            if (e.target.id == "signupPassword") {
                const errorMessage = checkValidPassword(e.target.value);
                if (errorMessage != "") {
                    setInputError(inputElement, errorMessage);
                } else {
                    clearInputError(inputElement);
                }
            // Check for password confirmation errors
            } else if (e.target.id == "signupConfirmPassword") {
                const isMatching = checkPasswordMatch();
                if (isMatching == false) {
                    setInputError(inputElement, "Passwords must match.");
                } else {
                    clearInputError(inputElement);
                }
            // Clear error for username or email address while the user is inputting
            } else {
                clearInputError(inputElement);
            }
        });
    });

    // Fires when the "Continue" button is clicked on login form
    loginForm.addEventListener("submit", e => {
        //e.preventDefault();
        //setFormMessage(loginForm, "error", "Invalid username or password");
    });

    // Fires when the "Continue" button is clicked on create account form
    createAccountForm.addEventListener("submit", e => {
        //e.preventDefault();
        //setFormMessage(createAccountForm, "error", "Invalid username or password");
    });
});